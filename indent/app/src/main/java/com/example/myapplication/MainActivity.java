package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView labelName;
    private EditText fieldName;
    private TextView labelFirstName;
    private EditText fieldFirstName;
    private Button envoi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        labelName=new TextView(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        labelName.setText("Nom");
        fieldName=new EditText(this);
        fieldName.setBackgroundColor(getResources().getColor(R.color.purple_200));
        labelFirstName=new TextView(this);
        labelFirstName.setText("Prenom");
        envoi=new Button(this);
        envoi.setText("Envoi");
        envoi.setBackgroundColor(getResources().getColor(R.color.purple_700));
        envoi.setTextColor(getResources().getColor(R.color.white));
        fieldFirstName=new EditText(this);
        fieldFirstName.setBackgroundColor(getResources().getColor(R.color.purple_200));
        LinearLayout mainLayout = findViewById(R.id.main_layout);
        mainLayout.addView(labelName);
        mainLayout.addView(fieldName);
        mainLayout.addView(labelFirstName);
        mainLayout.addView(fieldFirstName);
        LinearLayout.LayoutParams paramsButton = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsButton.setMargins(0,50,0,0);
        mainLayout.addView(envoi,paramsButton);
        envoi.setOnClickListener((e)->{
            String[] data = {fieldName.getText().toString(),fieldFirstName.getText().toString()};
            Intent intent = new Intent(this,ResultForm.class);
            intent.putExtra("data",data);
            startActivity(intent);
        });





    }
}