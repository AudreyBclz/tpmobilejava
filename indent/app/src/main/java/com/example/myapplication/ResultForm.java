package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ResultForm extends AppCompatActivity {

    private TextView resultName;

    private TextView resultFirstName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        resultName=new TextView(this);
        resultFirstName=new TextView(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_form);
        Intent intent = getIntent();
        String[] data = intent.getStringArrayExtra("data");
        resultName.setText(data[1]);
        resultFirstName.setText(data[0]);
        LinearLayout layout = findViewById(R.id.result_layout);
        layout.addView(resultFirstName);
        layout.addView(resultName);
    }
}