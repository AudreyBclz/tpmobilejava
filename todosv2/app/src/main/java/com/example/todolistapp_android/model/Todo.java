package com.example.todolistapp_android.model;

public class Todo {
    private int id;
    private String name;
    private boolean status;
    private boolean hasSubTask;

    public Todo(int id, String name, boolean status,boolean hasSubTask) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.hasSubTask=hasSubTask;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isHasSubTask() {
        return hasSubTask;
    }

    public void setHasSubTask(boolean hasSubTask) {
        this.hasSubTask = hasSubTask;
    }
}
