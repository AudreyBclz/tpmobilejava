package com.example.fragmenttp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.NavHostFragment;

import com.example.fragmenttp.databinding.FragmentFirstBinding;

public class FirstFragment extends Fragment {

    private FragmentFirstBinding binding;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentFirstBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.validButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText firstName = binding.firstnameEdittext;
                EditText lastName = binding.lastnameEdittext;
                EditText phone = binding.phoneEdittext;

                //NavDirections action = FirstFragmentDirections.actionFirstFragmentToSecondFragment(firstName.getText().toString(),lastName.getText().toString(),phone.getText().toString());
                NavDirections action1 = FirstFragmentDirections.actionFirstFragmentToListFragment(firstName.getText().toString(),lastName.getText().toString(),phone.getText().toString());
                /*NavHostFragment.findNavController(FirstFragment.this)
                        .navigate(action);*/
                NavHostFragment.findNavController(FirstFragment.this).navigate(action1);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}