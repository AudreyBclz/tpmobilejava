package com.example.todolistapp_android.adapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.example.todolistapp_android.dto.ResponseTodoDto;
import com.example.todolistapp_android.model.Todo;
import com.example.todolistapp_android.viewholder.TodoViewHolder;

public class TodosAdapter extends ListAdapter<ResponseTodoDto, TodoViewHolder> {

    private Fragment _fragment;
    public TodosAdapter(@NonNull DiffUtil.ItemCallback<ResponseTodoDto> diffCallback, Fragment fragment) {
        this(diffCallback);
        _fragment = fragment;
    }

    protected TodosAdapter(@NonNull DiffUtil.ItemCallback<ResponseTodoDto> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public TodoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return TodoViewHolder.create(parent, _fragment);
    }

    @Override
    public void onBindViewHolder(@NonNull TodoViewHolder holder, int position) {
        ResponseTodoDto todo = getItem(position);
        holder.display(todo, ()-> {notifyDataSetChanged();});
    }

    public static class TodoDiff extends DiffUtil.ItemCallback<ResponseTodoDto> {

        @Override
        public boolean areItemsTheSame(@NonNull ResponseTodoDto oldItem, @NonNull ResponseTodoDto newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull ResponseTodoDto oldItem, @NonNull ResponseTodoDto newItem) {
            return oldItem.getTitle().equals(newItem.getTitle()) && oldItem.isStatus() == newItem.isStatus();
        }
    }
}
