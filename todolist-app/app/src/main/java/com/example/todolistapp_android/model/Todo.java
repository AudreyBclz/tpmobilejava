package com.example.todolistapp_android.model;

import java.util.Date;

public class Todo {
    private int id;
    private String title;
    private String description;
    private boolean status;
    private Date date;

    public Todo(String title,String description, boolean status) {
        this.title = title;
        this.status = status;
        this.description=description;
        this.date=new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
