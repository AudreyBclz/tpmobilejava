package com.example.todolistapp_android.dto;

import java.util.Date;

public class RequestTodoDto {
    private String title;
    private String description;
    private boolean status;
    private Date date;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public RequestTodoDto(String title, String description) {
        this.title = title;
        this.description = description;
        this.status=false;
        this.date=new Date();
    }
}
