package com.example.todolistapp_android.service;

import com.example.todolistapp_android.dto.ResponseTodoDto;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface TodoServiceApi {

    @FormUrlEncoded
    @POST("/todo")
    Call<ResponseTodoDto> create (@Field("title")String title,@Field("description")String description);

    @GET("/todo")
    Call<List<ResponseTodoDto>> getAll();

    @GET("/todo/update/{id}")
    Call<ResponseTodoDto> updateStatus(@Path("id") int id);

    @GET("/todo/delete/{id}")
    Call<Boolean> delete(@Path("id")int id);

}
