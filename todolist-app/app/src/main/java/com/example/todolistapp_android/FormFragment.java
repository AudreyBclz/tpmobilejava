package com.example.todolistapp_android;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.example.todolistapp_android.databinding.FragmentFormBinding;
import com.example.todolistapp_android.dto.RequestTodoDto;
import com.example.todolistapp_android.dto.ResponseTodoDto;
import com.example.todolistapp_android.model.Todo;
import com.example.todolistapp_android.service.TodoService;
import com.example.todolistapp_android.service.TodoServiceApi;
import com.example.todolistapp_android.util.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FormFragment extends Fragment {

    FragmentFormBinding binding;
    TodoService todoService;
    TodoServiceApi _todoServiceApi;
    public FormFragment() {
        todoService = new TodoService();
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentFormBinding.inflate(inflater,container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.validButtton.setOnClickListener((e) -> {
            if(binding.nameEdittext.getText().length() > 3) {
                RequestTodoDto todo = new RequestTodoDto(binding.nameEdittext.getText().toString(),binding.descriptionEdittext.getText().toString());
                _todoServiceApi= RetrofitClient.getInstance().getRetrofit().create(TodoServiceApi.class);
               _todoServiceApi.create(binding.nameEdittext.getText().toString(),binding.descriptionEdittext.getText().toString()).enqueue(new Callback<ResponseTodoDto>() {

                   @Override
                   public void onResponse(Call<ResponseTodoDto> call, Response<ResponseTodoDto> response) {
                       ResponseTodoDto todoDto = response.body();
                       System.out.println(response);
                   }

                   @Override
                   public void onFailure(Call<ResponseTodoDto> call, Throwable t) {
                       System.out.println("fail");
                   }
               });
                //todoService.addTodo(binding.nameEdittext.getText().toString(),binding.descriptionEdittext.getText().toString());

                NavDirections action = FormFragmentDirections.actionFormToList();
                NavHostFragment.findNavController(FormFragment.this).navigate(action);
            }
            else {
                Toast.makeText(getContext(), "Merci de saisir un titre avec 4 caractères min", Toast.LENGTH_LONG).show();
            }
        });
    }
}