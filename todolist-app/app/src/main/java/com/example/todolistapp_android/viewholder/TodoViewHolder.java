package com.example.todolistapp_android.viewholder;

import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.todolistapp_android.R;
import com.example.todolistapp_android.TodosFragment;
import com.example.todolistapp_android.dto.ResponseTodoDto;
import com.example.todolistapp_android.model.Todo;
import com.example.todolistapp_android.service.TodoService;
import com.example.todolistapp_android.service.TodoServiceApi;
import com.example.todolistapp_android.util.RetrofitClient;

import org.w3c.dom.Text;

import java.util.List;
import java.util.function.Supplier;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TodoViewHolder extends RecyclerView.ViewHolder {

    private Fragment _fragment;
    private TextView nameTextView;
    private CheckBox statusCheckbox;
    private View view;
    private TextView descriptionTextView;
    private TextView dateTextView;
    private ImageButton deleteImage;
    private TodoService todoService;
    private TodoServiceApi _todoServiceApi;
    public TodoViewHolder(@NonNull View itemView, Fragment fragment) {
        this(itemView);
        _fragment = fragment;
        todoService = new TodoService();
    }

    public TodoViewHolder(@NonNull View itemView) {
        super(itemView);
        nameTextView = itemView.findViewById(R.id.todo_item_name_textview);
        descriptionTextView = itemView.findViewById(R.id.todo_item_description_textview);
        dateTextView = itemView.findViewById(R.id.todo_item_date_textview);
        statusCheckbox = itemView.findViewById(R.id.todo_item_done_checkbox);
        deleteImage =  itemView.findViewById(R.id.delete_item_img);
        view = itemView;
    }



    public void display(ResponseTodoDto todo, Runnable method) {
        nameTextView.setText(todo.getTitle());
        statusCheckbox.setChecked(todo.isStatus());
        statusCheckbox.setOnCheckedChangeListener((e, c)-> {
            _todoServiceApi= RetrofitClient.getInstance().getRetrofit().create(TodoServiceApi.class);
            _todoServiceApi.updateStatus(todo.getId()).enqueue(new Callback<ResponseTodoDto>() {
                @Override
                public void onResponse(Call<ResponseTodoDto> call, Response<ResponseTodoDto> response) {
                    ResponseTodoDto todo = response.body();
                }

                @Override
                public void onFailure(Call<ResponseTodoDto> call, Throwable t) {

                }
            });
            //todo.setStatus(c);
        });
        deleteImage.setOnClickListener((e) -> {
            _todoServiceApi = RetrofitClient.getInstance().getRetrofit().create(TodoServiceApi.class);
            _todoServiceApi.delete(todo.getId()).enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    TodosFragment.todos.remove(todo);
                    method.run();
                }

                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {

                }
            });
            //todoService.deleteTodo(todo);
        });
    }

    public static TodoViewHolder create(ViewGroup parent, Fragment fragment) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.todo_item, parent, false);
        return new TodoViewHolder(view, fragment);
    }
}
