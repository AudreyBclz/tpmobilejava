package com.example.todos.service;

import com.example.todos.model.Todo;

import java.util.ArrayList;
import java.util.List;

public class TodoService {

    static List<Todo> todos = new ArrayList<>();

    public List<Todo> getTodos(){
        return todos;
    }

    public void add(Todo todo){
        todos.add(todo);
    }

    public void update(int i, Todo newTodo){
        if(todos.get(i)!=null){
            todos.set(i,newTodo);
        }
    }
    public void delete(Todo todo){
        todos.remove(todo);
    }

}
