package com.example.todos.model;

public class Todo {
    private String title;
    private String description;
    private Boolean isDone;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getDone() {
        return isDone;
    }

    public void setDone(Boolean done) {
        isDone = done;
    }

    public Todo(String title, String description) {
        this.title = title;
        this.description = description;
        this.isDone=false;
    }
}
