package com.example.todos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.todos.databinding.FragmentFormBinding;
import com.example.todos.model.Todo;
import com.example.todos.service.TodoService;

public class FormTodoFragment extends Fragment {

    private FragmentFormBinding binding;

    private TodoService todoService;

    public FormTodoFragment(){
        this.todoService = new TodoService();
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentFormBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                todoService.add(new Todo(binding.title.getText().toString(),binding.description.getText().toString()));
                NavHostFragment.findNavController(FormTodoFragment.this)
                        .navigate(R.id.action_FormTodoFragment_to_ListTodosFragment);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}