package com.example.todos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.todos.adapter.TodoAdapter;
import com.example.todos.databinding.FragmentListBinding;

import com.example.todos.model.Todo;
import com.example.todos.service.TodoService;

public class listTodosFragment extends Fragment {

    private Todo todo;

    private FragmentListBinding binding;

    private TodoService todoService;

    private TodoAdapter todoAdapter;

    public listTodosFragment(){
        this.todoService = new TodoService();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceBundle){
        binding=FragmentListBinding.inflate(inflater,container,false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        todoAdapter = new TodoAdapter(new TodoAdapter.TodoDiff());
        binding.listTodos.setAdapter(todoAdapter);
        binding.listTodos.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.listTodos.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));
        todoAdapter.submitList(todoService.getTodos());


    }



}