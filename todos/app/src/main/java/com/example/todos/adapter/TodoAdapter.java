package com.example.todos.adapter;

import android.annotation.SuppressLint;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.example.todos.model.Todo;
import com.example.todos.viewHolder.TodoViewHolder;

import java.util.Objects;

public class TodoAdapter extends ListAdapter<Todo, TodoViewHolder> {


    public TodoAdapter(@NonNull DiffUtil.ItemCallback<Todo> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public TodoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return TodoViewHolder.create(parent);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull TodoViewHolder holder, int position) {
        Todo todo=getItem(position);
        holder.display(todo,(e)->{notifyDataSetChanged(); return null;});
    }

    public static class TodoDiff extends DiffUtil.ItemCallback<Todo>{

        @Override
        public boolean areItemsTheSame(@NonNull Todo oldItem, @NonNull Todo newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull Todo oldItem, @NonNull Todo newItem) {
            return oldItem.getTitle().equals(newItem.getTitle()) && oldItem.getDescription().equals(newItem.getDescription()) && Objects.equals(oldItem.getDone(), newItem.getDone());
        }
    }
}
