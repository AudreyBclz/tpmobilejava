package com.example.todos.viewHolder;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.SurfaceControl;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView;

import com.example.todos.R;
import com.example.todos.model.Todo;
import com.example.todos.service.TodoService;

import java.util.function.Function;

public class TodoViewHolder extends RecyclerView.ViewHolder {

    private TextView titleTextView;
    private TextView descriptionTextView;
    private Button updateButton;
    private Button deleteButton;
    private TodoService todoService;

    public TodoViewHolder(@NonNull View itemView) {
        super(itemView);
        todoService=new TodoService();
        titleTextView=itemView.findViewById(R.id.title_textview);
        descriptionTextView = itemView.findViewById(R.id.description_textview);
        updateButton=itemView.findViewById(R.id.update_button);
        deleteButton=itemView.findViewById(R.id.delete_button);
        itemView.setOnClickListener((e)->{
            if(updateButton.getVisibility()==View.VISIBLE){
                updateButton.setVisibility(View.GONE);
            }else{
                updateButton.setVisibility(View.VISIBLE);
            }
            if(deleteButton.getVisibility()==View.VISIBLE){
                deleteButton.setVisibility(View.GONE);
            }else{
                deleteButton.setVisibility(View.VISIBLE);
            }


        });
    }

    public void display(Todo todo, Function<Void,Void> method){
        titleTextView.setText(todo.getTitle());
        descriptionTextView.setText(todo.getDescription());
        if(todo.getDone()){
            updateButton.setText("To do");
        }else{
            updateButton.setText("Done");
        }
        updateButton.setOnClickListener((e)->{
            todo.setDone(!todo.getDone());
            todoService.update(getAdapterPosition(),todo);
            if(todo.getDone()){
                updateButton.setText("To do");
            }else{
                updateButton.setText("Done");
            }

        });
        deleteButton.setOnClickListener((e)->{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                method.apply(null);
            }
            todoService.delete(todo);
        });
    }

    public static  TodoViewHolder create(ViewGroup parent){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.todos_item,parent,false);
        return new TodoViewHolder(view);
    }
}
