package com.example.ecf_product.adapter;

import android.annotation.SuppressLint;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.example.ecf_product.model.ProductCart;
import com.example.ecf_product.viewholder.ProductOrderViewHolder;

public class ProductCartAdapter  extends ListAdapter<ProductCart, ProductOrderViewHolder> {
    private Fragment _fragment;

    public ProductCartAdapter(@NonNull DiffUtil.ItemCallback<ProductCart> diffCallback,Fragment fragment) {
        super(diffCallback);
        _fragment=fragment;
    }

    protected ProductCartAdapter(@NonNull DiffUtil.ItemCallback<ProductCart> diffCallback){
        super(diffCallback);
    }

    @NonNull
    @Override
    public ProductOrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       return ProductOrderViewHolder.create(parent,_fragment);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductOrderViewHolder holder, int position) {
        ProductCart productCart = getItem(position);
        holder.display(productCart,()->{notifyDataSetChanged();});
    }

    public static class ProductCartDiff extends  DiffUtil.ItemCallback<ProductCart>{

        @Override
        public boolean areItemsTheSame(@NonNull ProductCart oldItem, @NonNull ProductCart newItem) {
            return oldItem==newItem;
        }

        @SuppressLint("DiffUtilEquals")
        @Override
        public boolean areContentsTheSame(@NonNull ProductCart oldItem, @NonNull ProductCart newItem) {
            return oldItem.getProduct()==newItem.getProduct()
                    && oldItem.getQuantity()==newItem.getQuantity();
        }
    }
}
