package com.example.ecf_product.viewholder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecf_product.ListProductsFragment;
import com.example.ecf_product.ListProductsFragmentDirections;
import com.example.ecf_product.R;
import com.example.ecf_product.model.Product;
import com.example.ecf_product.service.ProductService;
import com.example.ecf_product.util.RetrofitClient;


public class ProductViewHolder extends RecyclerView.ViewHolder {
   private Fragment _fragment;
   private TextView productName;
   private View view;
   private ImageButton detailButton;
   private ProductService _productService;

    public ProductViewHolder(@NonNull View itemView, Fragment fragment) {
        this(itemView);
        _fragment=fragment;
        _productService= RetrofitClient.getInstance().getRetrofit().create(ProductService.class);
    }
    public ProductViewHolder(@NonNull View itemView){
        super(itemView);
        productName = itemView.findViewById(R.id.name_textview);
        detailButton = itemView.findViewById(R.id.detail_item_img);
        view=itemView;
    }
    public void display(Product product, Runnable method){
        productName.setText(product.getTitle());
        detailButton.setOnClickListener((e)->{
            NavDirections action = ListProductsFragmentDirections.actionListToDetail(product.getId());
            NavHostFragment.findNavController(_fragment).navigate(action);
        });
    }

    public static ProductViewHolder create(ViewGroup parent,Fragment fragment){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item,parent,false);
        return new ProductViewHolder(view,fragment);
    }
}
