package com.example.ecf_product.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.ecf_product.model.ProductOrder;

import java.util.List;

@Dao
public interface ProductOrderDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(ProductOrder productOrder);

    @Query("DELETE FROM PRODUCT_ORDER")
    void deleteAll();

    @Query("SELECT * FROM PRODUCT_ORDER")
    LiveData<List<ProductOrder>> getAll();

    @Query("SELECT * FROM PRODUCT_ORDER where productId=:id")
    LiveData<ProductOrder>findById(int id);

    @Query("UPDATE PRODUCT_ORDER SET quantity= :qty WHERE productId=:id")
    void update(int qty,int id);
}
