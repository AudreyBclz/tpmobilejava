package com.example.ecf_product.viewholder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecf_product.R;
import com.example.ecf_product.model.ProductCart;
import com.example.ecf_product.service.ProductService;
import com.example.ecf_product.util.RetrofitClient;

public class ProductOrderViewHolder extends RecyclerView.ViewHolder {
    private Fragment _fragment;
    private ProductService _productService;
    private TextView titleView;
    private TextView quantityView;
    private TextView priceView;
    private View view;

    public ProductOrderViewHolder(@NonNull View itemView,Fragment fragment) {
        this(itemView);
        _fragment=fragment;
        _productService = RetrofitClient.getInstance().getRetrofit().create(ProductService.class);
    }

    public ProductOrderViewHolder(@NonNull View itemView){
        super(itemView);
        titleView = itemView.findViewById(R.id.titleCart_textView);
        quantityView = itemView.findViewById(R.id.quantity_textView);
        priceView = itemView.findViewById(R.id.priceCart_textView);
        view = itemView;
    }
    public void display(ProductCart productCart,Runnable method){
        titleView.setText(productCart.getProduct().getTitle());
        quantityView.setText(String.valueOf(productCart.getQuantity()));
        priceView.setText(String.valueOf(productCart.getProduct().getPrice()* productCart.getQuantity()));
    }

    public static ProductOrderViewHolder create(ViewGroup parent, Fragment fragment){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item,parent,false);
        return new ProductOrderViewHolder(view,fragment);
    }
}
