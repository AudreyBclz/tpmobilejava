package com.example.ecf_product.util;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import com.example.ecf_product.dao.ProductOrderDao;
import com.example.ecf_product.model.ProductOrder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {ProductOrder.class},version = 2)
public abstract class ContextDataBase extends RoomDatabase {
    public abstract ProductOrderDao productOrderDao();

    private static ContextDataBase instance = null;

    public static ExecutorService databaseExecutor = Executors.newFixedThreadPool(3);

    public static ContextDataBase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), ContextDataBase.class, "product-order")
                    .addCallback(new Callback() {
                        @Override
                        public void onCreate(@NonNull SupportSQLiteDatabase db) {
                            super.onCreate(db);
                            databaseExecutor.execute(()->{
                                ProductOrderDao dao = instance.productOrderDao();
                                dao.deleteAll();
                            });
                        }
                    }).fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    @NonNull
    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @NonNull
    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

    @Override
    public void clearAllTables() {

    }
}
