package com.example.ecf_product.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.ecf_product.dao.ProductOrderDao;
import com.example.ecf_product.model.ProductOrder;
import com.example.ecf_product.util.ContextDataBase;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ProductOrderRepository {

    private ProductOrderDao _productOrderDao;
    public ProductOrderRepository(Application application){
        ContextDataBase db = ContextDataBase.getInstance(application);
        _productOrderDao = db.productOrderDao();
    }

    public void  insert(ProductOrder productOrder){
        ContextDataBase.databaseExecutor.execute(()->{
            _productOrderDao.insert(productOrder);
        });
    }

    public LiveData<ProductOrder> findProductOrderByProductId(int productId){
        return _productOrderDao.findById(productId);
    }

    public void update(int qty,int id){
        ContextDataBase.databaseExecutor.execute(()->{
            _productOrderDao.update(qty,id);
        });
    }

    public Completable insertRx(ProductOrder productOrder){
        return Completable.fromAction(()->{_productOrderDao.insert(productOrder);}).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public LiveData<List<ProductOrder>> getAll(){ return _productOrderDao.getAll();}
}
