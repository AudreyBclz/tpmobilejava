package com.example.ecf_product;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ecf_product.databinding.FragmentDetailBinding;
import com.example.ecf_product.model.Product;
import com.example.ecf_product.model.ProductOrder;
import com.example.ecf_product.repository.ProductOrderRepository;
import com.example.ecf_product.service.ProductService;
import com.example.ecf_product.util.RetrofitClient;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailFragment extends Fragment {

    private FragmentDetailBinding binding;
    private int productId;
    private Product product= null;
    private ProductService productService;
    private ImageView imageView;
    private TextView titleView;
    private TextView descriptionView;
    private TextView priceView;
    private ImageButton favoriteView;
    private ProductOrderRepository _repository;

    public DetailFragment() {
        // Required empty public constructor
        productService = RetrofitClient.getInstance().getRetrofit().create(ProductService.class);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _repository=new ProductOrderRepository(getActivity().getApplication());
        if (getArguments() != null) {
            DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
            productId = args.getId();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentDetailBinding.inflate(inflater,container, false);
        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view,Bundle savedInstanceState){
        if(!(productId ==0)){
            productService.getProductById(productId).enqueue(new Callback<Product>() {
                @Override
                public void onResponse(Call<Product> call, Response<Product> response) {
                    product = response.body();
                    System.out.println(product);
                    //ajout des données produit pour les afficher
                    titleView=binding.titleTextView;
                    titleView.setText(product.getTitle());
                    imageView=binding.detailImageView;
                    //Utilisation de Picasso pour afficher une image grâce à son URL
                    Picasso.get().load(product.getImage()).into(imageView);
                    descriptionView=binding.descriptionTextView;
                    descriptionView.setText(product.getDescription());
                    priceView=binding.priceTextView;
                    priceView.setText(String.valueOf(product.getPrice()+" $"));
                    favoriteView=binding.favoriteButton;
                    favoriteView.setOnClickListener((e)->{
                        LiveData<ProductOrder> observable = _repository.findProductOrderByProductId(product.getId());
                        observable.observe(getActivity(),(productOrder)->{
                            if(productOrder!=null){
                                int newQty =productOrder.getQuantity()+1;
                                System.out.println(newQty);
                                observable.removeObservers(getActivity());
                                _repository.update(newQty,product.getId());
                            }else{
                                observable.removeObservers(getActivity());
                                _repository.insert(new ProductOrder(productId,1));
                            }
                        });
                    });

                }
                    @Override
                public void onFailure(Call<Product> call, Throwable t) {

                }
            });
        }
    }
}