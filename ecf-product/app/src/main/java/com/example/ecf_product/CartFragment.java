package com.example.ecf_product;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ecf_product.adapter.ProductCartAdapter;
import com.example.ecf_product.databinding.FragmentCartBinding;
import com.example.ecf_product.model.Product;
import com.example.ecf_product.model.ProductCart;
import com.example.ecf_product.model.ProductOrder;
import com.example.ecf_product.repository.ProductOrderRepository;
import com.example.ecf_product.service.ProductService;
import com.example.ecf_product.util.ContextDataBase;
import com.example.ecf_product.util.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartFragment extends Fragment {
    private FragmentCartBinding binding;
    private List<ProductCart> productCarts;
    private ProductCartAdapter productCartAdapter;
    private ProductService _productService;
    private ProductOrderRepository _repository;
    private List<ProductOrder>orders;
    private Product product;

    public CartFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _productService= RetrofitClient.getInstance().getRetrofit().create(ProductService.class);
        _repository = new ProductOrderRepository(getActivity().getApplication());
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentCartBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle saveInstanceState){
        productCartAdapter = new ProductCartAdapter(new ProductCartAdapter.ProductCartDiff(),CartFragment.this);
        binding.listCart.setAdapter(productCartAdapter);
        binding.listCart.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.listCart.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));
        productCarts=new ArrayList<>();
        productCartAdapter.submitList(productCarts);
         _repository.getAll().observe(getActivity(), orders1 -> {
            orders=orders1;
            makeListProductCart(orders);

        });
        System.out.println(orders);

    }

    private void makeListProductCart(List<ProductOrder> orders){
        if(orders.size()>0){
            for(ProductOrder o: orders){
                 _productService.getProductById(o.getProductId()).enqueue(new Callback<Product>() {
                    @Override
                    public void onResponse(Call<Product> call, Response<Product> response) {
                        product=response.body();
                        ProductCart cart= new ProductCart(product,o.getQuantity());
                        productCarts.add(cart);
                        productCartAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(Call<Product> call, Throwable t) {

                    }
                });

            }
        }
    }
}