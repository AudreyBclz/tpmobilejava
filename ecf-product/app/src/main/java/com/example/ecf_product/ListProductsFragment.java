package com.example.ecf_product;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ecf_product.adapter.ProductAdapter;
import com.example.ecf_product.databinding.FragmentListProductsBinding;
import com.example.ecf_product.model.Product;
import com.example.ecf_product.service.ProductService;
import com.example.ecf_product.util.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListProductsFragment extends Fragment {

    private FragmentListProductsBinding binding;
    private ProductAdapter productAdapter;
    private ProductService productService;
    private List<Product> products;


    public ListProductsFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding= FragmentListProductsBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){
        productAdapter = new ProductAdapter(new ProductAdapter.TodoDiff(),ListProductsFragment.this);
        binding.productsRecyclerview.setAdapter(productAdapter);
        binding.productsRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.productsRecyclerview.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));
        productService= RetrofitClient.getInstance().getRetrofit().create(ProductService.class);
        productService.getAll().enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                products=response.body();
                productAdapter.submitList(products);
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }
}