package com.example.ecf_product.service;

import com.example.ecf_product.model.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ProductService {

    @GET("/products")
    Call<List<Product>>getAll();

    @GET("/products/{id}")
    Call<Product>getProductById(@Path("id")int id);
}
