package com.example.ecf_product.model;

public class ProductCart {
    private Product product;
    private int quantity;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public ProductCart(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }
}
