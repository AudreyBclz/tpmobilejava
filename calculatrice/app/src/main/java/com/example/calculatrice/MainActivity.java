package com.example.calculatrice;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Boolean isNewNumber = true;
    private Double oldNumber = null;
    String lastOperation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TableLayout calculatriceLayout = findViewById(R.id.calculatrice_layout);
        for(int i =1; i<calculatriceLayout.getChildCount();i++){

            TableRow row = (TableRow) calculatriceLayout.getChildAt(i);
            for(int j=0; j<row.getChildCount();j++){
                Button b = (Button)row.getChildAt(j);
                b.setOnClickListener((e)->{
                    actionButton(b);

                });
            }
        }
    }

   private void actionButton(Button b) {

       TextView aff = findViewById(R.id.affichage);

       String content = (String) b.getText();
       try {
           double number = Double.valueOf(content);
           if (isNewNumber) {
               aff.setText(content);
               isNewNumber = false;
           } else {
               aff.setText(aff.getText() + "" + content);
           }
       } catch (Exception ex) {
           double number = Double.valueOf((String) aff.getText());
           if (oldNumber == null) {
               oldNumber = number;
           } else {
               switch (lastOperation) {
                   case "+":
                       oldNumber = oldNumber + number;
                       break;
               }
               aff.setText(oldNumber.toString());
           }
           lastOperation = content;
           isNewNumber = true;

       }
   }
}